# Docker config

- Deploy branch: `main`
- File env for deploy: `.env.main`

# Use

Copy all file in a folder (cra, nestjs or nextjs) to your project.

Example:

```bash
cp -r cra/* /path/to/your/project
```
